package com.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import com.university.model.course.Course;
import com.university.model.misc.Address;
import com.university.model.misc.Phone;
import com.university.model.role.Student;

public class StudentDAO {
public StudentDAO() {}
	
	public Student getStudent(int studentID,String name) {
		 	 
	    try { 		
	    	//Get Customer
	    	Statement st = JDBCMySQLConnection.getConnection().createStatement();
	    	String selectStudentQuery = "SELECT studentID, lname, fname, addressID, phoneID, courseID, bill FROM Student WHERE studentID = '" + studentID + "' and fname = '" + name + "'";

	    	ResultSet studentRS = st.executeQuery(selectStudentQuery);      
	    	System.out.println("StudentDAO: *************** Query " + selectStudentQuery);
	    	
	      //Get Customer
    	  Student student = new Student();
	      if ( studentRS.next() ) {
	    	  student.setId(studentRS.getInt("studentID"));
	    	  student.setFname(studentRS.getString("fname"));
	    	  student.setLname(studentRS.getString("lname"));
	    	  student.setStudentBill(studentRS.getDouble("bill"));
	    	  
	      	    		  
		      //Get Address
		      String selectAddressQuery = "SELECT  addressType,address1, address2, city, state, zip FROM Address WHERE addressID = '" + studentRS.getString("addressId") + "'";
		      ResultSet addRS = st.executeQuery(selectAddressQuery);
	    	  
		      
	    	  System.out.println("StudentDAO: *************** Query " + selectAddressQuery);
	    	  Address address = new Address();
		      while ( addRS.next() ) {
		    	  address = new Address();
		    	  address.setAddressType(addRS.getInt("addressType"));
		    	  address.setAddress1(addRS.getString("address1"));
		    	  address.setAddress2(addRS.getString("address2"));
		    	  address.setCity(addRS.getString("city"));
		    	  address.setState(addRS.getString("state"));
		    	  address.setZipcode(addRS.getString("zip"));
		    	  student.addStudentAddress(address);
		    	  
		      }
		      addRS.close();
		      String selectPhoneQuery = "SELECT  phoneType,number FROM Phone WHERE phoneID = '" + studentRS.getString("phoneId") + "'";
		      ResultSet phoneRS = st.executeQuery(selectPhoneQuery);
	    	  
		      
	    	  System.out.println("StudentDAO: *************** Query " + selectPhoneQuery);
	    	  Phone phone = new Phone();
		      while ( phoneRS.next() ) {
		    	  phone = new Phone();
		    	  phone.setPhoneType(phoneRS.getString("phoneType"));
		    	  phone.setPhoneNumber(phoneRS.getInt("number"));
		    	  student.addStudentPhone(phone);
	   	  
		      }
		      
		      phoneRS.close();
		      
	      //close to manage resources

	      }
	      //close to manage resources
	      studentRS.close();
	      
	      st.close();
	      
	      return student;
	    }	    
	    catch (SQLException se) {
	      System.err.println("StudentDAO: Threw a SQLException retrieving the student object.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }
	    
	    return null;
	  }
	
	public int addStudent(Student student) {
		Connection con = JDBCMySQLConnection.getConnection();
        PreparedStatement studentPst = null;
        PreparedStatement addPst = null;
        PreparedStatement phonePst = null;
        String addStm = null;
        try {
        	//Insert the customer object
            String studentStm = "INSERT INTO Student(lname, fname, bill) VALUES(?, ?, ?)";
            studentPst = con.prepareStatement(studentStm);
            studentPst.setString(2, student.getLname());       
            studentPst.setString(3, student.getFname()); 
            studentPst.setDouble(1, student.getStudentBill());
            studentPst.executeUpdate();

            
            Iterator<Address> it = student.getStudentAddress().iterator();
            while(it.hasNext()){
            	Address address = it.next();
            
            
        	//Insert the customer address object
	            addStm = "INSERT INTO Address(addressID, addressType, address1, address2, city, state, zip) VALUES(?, ?, ?, ?, ?, ?, ?)";
	            addPst = con.prepareStatement(addStm);
	            addPst.setInt(2, address.getAddressType()); 
	            addPst.setString(3, address.getAddress1());  
	            addPst.setString(4, address.getAddress2());         
	            addPst.setString(5, address.getCity());  
	            addPst.setString(6, address.getState());      
	            addPst.setString(7, address.getZipcode());  
	            addPst.executeUpdate();
	            
	            
            }
            
            return student.getId();
        } catch (SQLException ex) {

        	return (Integer) null;
        } finally {

            try {
                if (addPst != null) {
                	addPst.close();
                	studentPst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
      	      System.err.println("StudentDAO: Threw a SQLException saving the student object.");
    	      System.err.println(ex.getMessage());
            }
        }
    }

}

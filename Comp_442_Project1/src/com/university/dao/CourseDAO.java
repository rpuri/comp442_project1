package com.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.university.model.course.Course;

public class CourseDAO {
 public CourseDAO(){};
 
 public Course getCourse(String courseName){
	 try {
		 
		 Statement st = JDBCMySQLConnection.getConnection().createStatement();
	    	String selectCourseQuery = "SELECT courseID, coursename, semester, professor FROM Course WHERE coursename = '" + courseName + "'";

	    	ResultSet courseRS = st.executeQuery(selectCourseQuery);      
	    	System.out.println("CourseDAO: *************** Query " + selectCourseQuery);

		 Course course = new Course();
		 if(courseRS.next()){
			 course.setCourseID(courseRS.getInt("courseID"));
			 course.setCourseName(courseRS.getString("coursename"));
			 course.setCourseProfessor(courseRS.getString("professor"));
			 course.setCourseSemester(courseRS.getString("semester"));
		 }
		 
		 courseRS.close();
		 st.close();
		 return course;	 
	 }
	 catch (SQLException se) {
	      System.err.println("CourseDAO: Threw a SQLException retrieving the course object.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }
	 return null;
 }
 
 
 public void addCourse(Course course) {
	Connection con = JDBCMySQLConnection.getConnection();
     PreparedStatement coursePst = null;


     try {
     	//Insert the course object
         String courseStm = "INSERT INTO Course(courseID, coursename, semester, professor) VALUES(?, ?, ?, ?)";
         coursePst = con.prepareStatement(courseStm);
         coursePst.setInt(1, course.getCourseID());
         coursePst.setString(2, course.getCourseName());
         coursePst.setString(3, course.getCourseSemester());       
         coursePst.setString(4, course.getCourseProfessor()); 
         coursePst.executeUpdate();

          } catch (SQLException ex) {

     } finally {

         try {
             if (coursePst != null) {
             	coursePst.close();
             	
             }
             if (con != null) {
                 con.close();
             }

         } catch (SQLException ex) {
   	      System.err.println("CourseDAO: Threw a SQLException saving the course object.");
 	      System.err.println(ex.getMessage());
         }
     }
 }
 
 
}

package com.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.university.model.course.Course;
import com.university.model.role.Professor;

public class ProfDAO {
 public ProfDAO(){};
  
 	public void  addCourse(Course course){
 		Connection con = JDBCMySQLConnection.getConnection();
 	     PreparedStatement coursePst = null;


 	     try {
 	     	//Insert the course object
 	         String courseStm = "INSERT INTO Course(courseID, coursename, semester, professor) VALUES(?, ?, ?, ?)";
 	         coursePst = con.prepareStatement(courseStm);
 	         coursePst.setInt(1, course.getCourseID());
 	         coursePst.setString(2, course.getCourseName());
 	         coursePst.setString(3, course.getCourseSemester());       
 	         coursePst.setString(4, course.getCourseProfessor()); 
 	         coursePst.executeUpdate();

 	          } catch (SQLException ex) {

 	     } finally {

 	         try {
 	             if (coursePst != null) {
 	             	coursePst.close();
 	             	
 	             }
 	             if (con != null) {
 	                 con.close();
 	             }

 	         } catch (SQLException ex) {
 	   	      System.err.println("ProfDAO: Threw a SQLException saving the Professor object.");
 	 	      System.err.println(ex.getMessage());
 	         }
 	     }
 	 }

 		
}

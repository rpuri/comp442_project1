package com.university.model.role;

import java.util.ArrayList;
import java.util.List;

import com.university.model.course.Course;
import com.university.model.misc.Email;


public class Professor extends Actor {

	private List<Course> courses = new ArrayList<Course>();
	private List<Email> emails = new ArrayList<Email>();
	
	public Professor(){}
	
	public List<Course> getCourse() {
	
		return courses;
	}
	
	
	public void setCourse(List<Course> courses) {
		this.courses = courses;
	}

	public void addCourse(Course course) {
		this.courses.add(course);
	}
	
	public List<Email> getEmail() {
		return emails;
	}
	
	public void setEmail(List<Email> emails) {
		this.emails = emails;
	}
	
	public void addEmail(Email email) {
		this.emails.add(email);
	}
}

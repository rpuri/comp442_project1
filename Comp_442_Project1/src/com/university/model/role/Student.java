package com.university.model.role;

import java.util.ArrayList;
import java.util.List;

import com.university.model.course.CourseGrade;
import com.university.model.misc.Address;
import com.university.model.misc.Email;
import com.university.model.misc.Phone;

public class Student extends Actor {

	private List<Address> studentAddressList = new ArrayList<Address>();
	private List<Phone> studentPhoneList = new ArrayList<Phone>();
	private List<Email> studentEmailList = new ArrayList<Email>();
	private List<CourseGrade> studentCourseGradeList = new ArrayList<CourseGrade>();
	private double studentBill;
	
	public Student(){}
	
	public List<Address> getStudentAddress() {
		return studentAddressList;
	}
	public void setStudentAddress(List<Address> studentAddress) {
		this.studentAddressList = studentAddress;
	}
	
	public void addStudentAddress(Address studentAddress) {
		this.studentAddressList.add(studentAddress);
	}
	public List<Phone> getStudentPhone() {
		return studentPhoneList;
	}
	public void setStudentPhone(List<Phone> studentPhone) {
		this.studentPhoneList = studentPhone;
	}
	
	public void addStudentPhone(Phone studentPhone) {
		this.studentPhoneList.add(studentPhone);
	}
	public List<Email> getStudentEmail() {
		return studentEmailList;
	}
	public void setStudentEmail(List<Email> studentEmail) {
		this.studentEmailList = studentEmail;
	}
	
	public void addStudentEmail(Email studentEmail) {
		this.studentEmailList.add(studentEmail);
	}
	public List<CourseGrade> getStudentCourseGrade() {
		return studentCourseGradeList;
	}
	
	public void setStudentCourseGrade(List<CourseGrade> studentCourseGrade) {
		this.studentCourseGradeList = studentCourseGrade;
	}
	
	public void addStudentCourseGrade(CourseGrade studentCourseGrade) {
		this.studentCourseGradeList.add(studentCourseGrade);
	}
	
	public double getStudentBill() {
		return studentBill;
	}
	public void setStudentBill(double studentBill) {
		this.studentBill = studentBill;
	}
	
}

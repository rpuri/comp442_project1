package com.university.model.service;

import com.university.dao.StudentDAO;
import com.university.model.role.Registrar;
import com.university.model.role.Student;

public class RegistrarService {
	
	private Registrar registrar = new Registrar();
	private StudentDAO studentDAO = new StudentDAO();
	public RegistrarService(){}
	
	public int createStudent(Student student){
		return studentDAO.addStudent(student);
		//this.registrar.setCreateStudent(student);
		//return this.registrar.getCreateStudent().getId();
	}
	
	public Student searchStudent(int id, String name){
		Student student = studentDAO.getStudent(id, name);
		//add dao call to search student		
		return student;
	}
}

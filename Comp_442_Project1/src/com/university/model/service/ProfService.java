package com.university.model.service;

import java.util.Iterator;

import com.university.dao.ProfDAO;
import com.university.model.course.Course;
import com.university.model.role.Professor;

public class ProfService {
	
	private Professor prof = new Professor();
	private Course course = new Course();
	
	private ProfDAO profDAO = new ProfDAO();
	
	public ProfService(){}
	public ProfService(Professor professor){
		this.prof.setCourse(professor.getCourse());
		this.prof.setEmail(professor.getEmail());
		this.prof.setFname(professor.getFname());
		this.prof.setLname(professor.getLname());
		this.prof.setId(professor.getId());
	}
	
	public void addCourse(Course course){
		
		profDAO.addCourse(course);
		//this.prof.addCourse(course);		
	}
	/*
	public Course searchCourse(String courseName){
			Iterator<Course> it = prof.getCourse().iterator();
			while(it.hasNext()){
				Course objCourse = it.next();
				if (objCourse.getCourseName().equals(courseName)){
					return objCourse;
				}
			}
			return course;
	}
	*/
}

package com.university.model.service;


import com.university.dao.CourseDAO;
import com.university.model.course.Course;

public class CourseService {
	private CourseDAO courseDAO = new CourseDAO();
	//private Course course = new Course();
	public CourseService(){}
	
	public Course searchCourse(String courseName){
		//add logic to call Dao with the name  -- next project
		Course course = courseDAO.getCourse(courseName);
		return course;
	}
	
	public void saveCourse(Course course){
		
		//add logic to call Dao to store the course in database -- next project
		courseDAO.addCourse(course);
	}
}

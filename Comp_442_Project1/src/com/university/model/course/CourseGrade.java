package com.university.model.course;

public class CourseGrade extends Course {

	private String courseGrade;
	
	public CourseGrade(){}

	public String getCourseGrade() {
		return courseGrade;
	}

	public void setCourseGrade(String courseGrade) {
		this.courseGrade = courseGrade;
	}
			
}

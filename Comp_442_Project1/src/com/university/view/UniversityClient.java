package com.university.view;

import com.university.model.course.Course;
import com.university.model.course.CourseGrade;
import com.university.model.misc.*;
import com.university.model.role.Professor;
import com.university.model.role.Student;
import com.university.model.service.CourseService;
import com.university.model.service.ProfService;
import com.university.model.service.RegistrarService;

public class UniversityClient {
	
	public static void main (String args[]) throws Exception {
		
		RegistrarService registrarService = new RegistrarService();
		
		//Lets create a new student
		Student student = new Student();
		student.setFname("Rohit");
		student.setLname("Puri");
		Address billingAddress = new Address();
		billingAddress.setAddressType(1);
        billingAddress.setAddress1("1425 S wolf rd.");
        billingAddress.setAddress2("Apt 007");
        billingAddress.setCity("Chicago");
        billingAddress.setState("IL");
        billingAddress.setZipcode("60070");
        student.addStudentAddress(billingAddress);
		
        Email email = new Email();
        email.setEmailType(1);
        email.setEmailId("rpuri@luc.edu");
        student.addStudentEmail(email);
        
        CourseGrade courseGrade = new CourseGrade();
        courseGrade.setCourseName("Server Side Programming");
        courseGrade.setCourseSemester("Fall 2013");
        courseGrade.setCourseProfessor("Prof. Zewdie");
        student.addStudentCourseGrade(courseGrade);
        
        //Now adding the student to the university
        
        int studentId = registrarService.createStudent(student);
        
        System.out.println("A new student is created in system with Student ID :" + studentId + "\n");
        
        
        // We can also add a course
        CourseService courseService = new CourseService();
        
        Course course = new Course();
        course.setCourseName("Client Side Programming");
        course.setCourseProfessor(" Prof Albert");
        course.setCourseSemester("FAll 2013");
        
        courseService.saveCourse(course);
        System.out.println("New Course Successfully added");
        System.out.println("Course Name :" + courseService.searchCourse("Client Side Programming").getCourseName());
        System.out.println("Professor" + courseService.searchCourse("Client Side Programming").getCourseProfessor());
        System.out.println("Semester" + courseService.searchCourse("Client Side Programming").getCourseSemester() + "\n");
        
        //We can also add courses to a professor
        
        Professor professor = new Professor();
        professor.setFname("Briggs");
        professor.setLname("Patrick");
        professor.setId(111);
        //professor.addCourse(course);
        professor.addEmail(email);
        
        ProfService profService = new ProfService(professor);
        profService.addCourse(course);
        System.out.println("New Course attached to Professor");
       // System.out.println("Course Name :" + profService.searchCourse("Client Side Programming").getCourseName());
       // System.out.println("Semester" + profService.searchCourse("Client Side Programming").getCourseSemester() + "\n");
        
		
	}

}
